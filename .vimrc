syntax enable
retab                         " convert all tabs to spaces

" Centralize backups, swapfiles and undo history
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
if exists("&undodir")
  set undodir=~/.vim/undo
endif   

" Use the OS clipboard by default (on versions compiled with `+clipboard`)
set clipboard=unnamed
" Enhance command-line completion
set wildmenu
" Allow cursor keys in insert mode
set esckeys

set binary
set noeol
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list
set incsearch
set hlsearch
set mouse=a
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent smartindent    " auto/smart indent
set smarttab                  " tab and backspace are smart
set background=dark
set ruler                     " show the line number on the bar
set more                      " use more prompt
set autoread                  " watch for file changes
set number                    " line numbers
set hidden
set noautowrite               " don't automagically write on :next
set lazyredraw                " don't redraw when don't have to
set showmode
set showcmd
set scrolloff=999             " keep at least 5 lines above/below
set sidescrolloff=999         " keep at least 5 lines left/right
set backspace=indent,eol,start
set whichwrap+=<,>,[,]        " cursor left and right move to next/prev lines
set linebreak
set complete=.,w,b,u,U,t,i,d  " do lots of scanning on tab completion
set ttyfast                   " we have a fast terminal
filetype on                   " Enable filetype detection
filetype indent on            " Enable filetype-specific indenting
filetype plugin on            " Enable filetype-specific plugins
highlight Normal ctermfg=white ctermbg=black

